#
# Be sure to run `pod lib lint tianma_live.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'tianma_live'
  s.version          = '0.3.7'
  s.summary          = ' this is the tianma_live framework  for tianma.   '

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
                          a framework for tianma live.a framework for tianma live .
                       DESC

  s.homepage         = 'https://gitee.com/livea01/livea01_ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '360tianma@higgses.com' => '360tianma@higgses.com' }
  s.source           = { :git => 'https://gitee.com/livea01/livea01_ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  #s.source_files = 'tianma_live/Classes/*'
  s.source_files = 'tianma_live/**/*.framework/Headers/*'
  s.ios.vendored_frameworks = 'tianma_live/**/*.framework'

  #s.xcconfig = {'VALID_ARCHS' => 'arm64' 'arm64e' 'armv7' 'armv7s'}
  s.pod_target_xcconfig = { 'VALID_ARCHS[sdk=iphonesimulator*]' => '' }
  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = "Foundation","UIKit"

  s.dependency "TMSDK"
  s.dependency "TMUserCenter"
  s.dependency "AFNetworking"
  s.dependency "MBProgressHUD"
  s.dependency "JSONModel"
  s.dependency "Masonry"
  s.dependency "MJRefresh"
  s.dependency "SDWebImage"
  s.dependency "YYCache"
  s.dependency "IQKeyboardManager"
  s.dependency "QBImagePickerController", '~> 3.4.0'
  s.dependency "MJExtension"
  s.dependency 'IJKMediaFramework' 
       


end
