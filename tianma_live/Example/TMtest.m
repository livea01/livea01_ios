//
//  TMtest.m
//  tianma_live_Tests
//
//  Created by 刘晓霖 on 2019/3/31.
//  Copyright © 2019 274947394@qq.com. All rights reserved.
//

#import "TMtest.h"
#import <TianmaFramework/VideoPlayBackVC.h>
@implementation TMtest
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    self presentViewController:[VideoPlayBackVC new] animated:YES completion:nil];
}
@end
